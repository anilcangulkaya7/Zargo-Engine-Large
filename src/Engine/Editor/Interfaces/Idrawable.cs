﻿

namespace ZargoEngine.Editor
{
    // this is for Imguı editor drawing
    public interface IDrawable
    {
        public void DrawWindow();
    }
}
